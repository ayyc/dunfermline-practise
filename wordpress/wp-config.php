<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dunfermline_practise_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'k>qtO]s=G7[K!z[o^RpBI4sR/+wuhaQ2tvEV;A!^ERjfPPg<~,=$]/2r^nRhl= L' );
define( 'SECURE_AUTH_KEY',  '38&1tdUM`HuWI9$:[-.YuY?^yIG7SwBh$ZO,i02 }a9P3W,Uq?>_3A@fgeWSxG;o' );
define( 'LOGGED_IN_KEY',    'e[%{b}7 BO,=(c1c}bHgDL3k ]ueHNLQbE_2gn&}$tTX$YF:5VAdzY_OESoa#-SR' );
define( 'NONCE_KEY',        'ksO=pPgl$/Awo+zs3`WUuWTPKr|gi7Gio[}O(%_RbcgrevN#r:/8YNeF[exp(]IR' );
define( 'AUTH_SALT',        'a5+PG}|~zjDX7#B,hGD{Nz_f$A3!.Wz*oP8_:/|PgdU-br[Ib^]~((Pqa]YF_2-8' );
define( 'SECURE_AUTH_SALT', '<RB14bS#4]^,! OlQE5Q^!n|$j0kx98M_?-5?[GI;ah!H7q)+VmuVTvx1swKe4-!' );
define( 'LOGGED_IN_SALT',   'c22_LdahYmsG-d=l@iiv-Xy`vp[P>s(fnOts2rfLjK-bVd6n1O!Z]J#-j1g:0:VU' );
define( 'NONCE_SALT',       ',BnWqmE$_yNe]Us^b[ 6|a@MefX{R9-a:?f8WBP9G009qVboV  :b8n8)9xr>}6K' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
